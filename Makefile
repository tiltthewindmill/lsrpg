JCC=javac
 
BIN_DIR=bin
SRC_DIR=lsrpg

FLAGS= -d $(BIN_DIR)

DUNGEON_SOURCES = \
	$(SRC_DIR)/dungeon/Dungeon.java

PLAYER_SOURCES = \
	$(SRC_DIR)/player/Player.java \
	$(SRC_DIR)/player/Classes.java \
	$(SRC_DIR)/player/Inventory.java 

COMMON_SOURCES = \
	$(SRC_DIR)/common/ArtPrinter.java \
	$(SRC_DIR)/Game.java \
	$(SRC_DIR)/common/Constants.java \
	$(SRC_DIR)/common/Menu.java \
	$(SRC_DIR)/common/Modifier.java \
	$(SRC_DIR)/LSRPG.java \
	$(SRC_DIR)/GUI.java \
	$(SRC_DIR)/common/Item.java \
	$(SRC_DIR)/common/ItemFactory.java

BEAST_SOURCES = \
	$(SRC_DIR)/beast/Beast.java \
	$(SRC_DIR)/beast/BeastFactory.java

BATTLE_SOURCES = \
	$(SRC_DIR)/battle/Combat.java \
	$(SRC_DIR)/battle/Battle.java \
	$(SRC_DIR)/battle/Attack.java

all: dungeon player common beast battle

dungeon: $(DUNGEON_SOURCES)
	$(JCC) $(FLAGS) $< $(DUNGEON_SOURCES)

player: $(PLAYER_SOURCES)
	$(JCC) $(FLAGS) $< $(PLAYER_SOURCES)

common: $(COMMON_SOURCES)
	$(JCC) $(FLAGS) $< $(COMMON_SOURCES)

beast: $(BEAST_SOURCES)
	$(JCC) $(FLAGS) $< $(BEAST_SOURCES)

battle: $(BATTLE_SOURCES)
	$(JCC) $(FLAGS) $< $(BATTLE_SOURCES)
jar: *
	jar cfvm LSRPG.jar manifest.txt -C bin/lsrpg/*
clean:
	rm $(BIN_DIR)/lsrpg/*.class
	rm $(BIN_DIR)/lsrpg/dungeon/*.class
	rm $(BIN_DIR)/lsrpg/player/*.class
	rm $(BIN_DIR)/lsrpg/beast/*.class
	rm $(BIN_DIR)/lsrpg/common/*.class
	rm $(BIN_DIR)/lsrpg/battle/*.class
	rm LSRPG.jar
