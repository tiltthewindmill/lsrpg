#Do a full release compile and build jar
mkdir release
make clean
make all
cd bin
jar cfvm LSRPG.jar lsrpg/manifest.txt -C lsrpg/*
jar uf LSRPG.jar lsrpg/Game.class
jar uf LSRPG.jar lsrpg/GUI.class
cp LSRPG.jar ../release
cd ..