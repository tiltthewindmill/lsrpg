package lsrpg.player;
import lsrpg.*;
import lsrpg.battle.*;
import lsrpg.common.*;

import java.util.ArrayList;
public interface Player{

	public int getHealth();
	public int getStrength();
	public int getMagic();
	public int getMagicDamage();
	public int getResistance();
	public String getClassName();

	public ArrayList<Attack> getAttacks();
	public void returnMaxHealth();
	public void setPlayerName(String s);
	public String getPlayerName();

	public void setHealth(int health);
	public void subtractHealth(int amt);

	public String statPrinter();

	public Inventory getInventory();
	public String printInventory();
	public String printSideInventory();
	public String useItem (String s);
	public void addItem(Item item);


	//public Item getWeapon();
	//public Item getArmour();


	//These allow for user defined class creation.
	//Granted these will have to be parsed in.
	//Remove for now OR do class creation on the fly?
	//On the fly is a terrible idea. Big wall of ifs.
	/*public void setHealth(int h);
	public void setStrength(int s);
	public void setMagic(int m);
	public void setMagicDamage(int md);
	public void setResistance(int r);
	public void setClassName(String cl);
	public void newAttack(Attack a);*/


}