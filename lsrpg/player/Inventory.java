package lsrpg.player;

import lsrpg.*;
import lsrpg.common.*;
import lsrpg.player.*;
import java.util.*;


public class Inventory{
	Player player;
	HashSet<Item> inventory = new HashSet<Item>();
	ArrayList<String> done = new ArrayList<String>();	//For printing purposes
	int maxItemNameLength;
	

	public Inventory(Player player){
		this.player = player;
	}

	public String print(){
		String out = "";
		out += "Inventory:\n";
		for (Item item : inventory){
			if (!done.contains(item.getItemName())){
				out += spacer(item.getItemName()) +"Amount: "+getCount(item);
				if (item.getModifier() != null){
					if (item.getModifier().getAmount() < 0)
						out += "    Modifier: -"+ item.getModifier().getAmount() + "    " + item.getModifier().getType();
					else 
						out += "    Modifier: +"+ item.getModifier().getAmount() + "    " + item.getModifier().getType();
				}
				out += "\n";
				done.add(item.getItemName());
			}

		}
		done = new ArrayList<String>();
		return out;
	}

	public String sideBarPrint(){
		String out = "";
		for (Item item : inventory){
			if (!done.contains(item.getItemName())){
				out += spacerSide(item.getItemName()) +"#: "+getCount(item) + "\n";
				done.add(item.getItemName());
			}
		}
		done = new ArrayList<String>();
		return out;
	}

	public int getCount(Item item){
		int count = 0;
		for (Item it : inventory){
			if (it.getItemName().equals(item.getItemName())){
				count ++;
			}
		}
		return count;
	}

	public void addItem(String name, int count, Modifier modifier){
		for (int i = 0; i < count; i++){
			inventory.add(new Item(name, modifier));
			if (name.length() > maxItemNameLength)
				maxItemNameLength = name.length();
		}
	}

	public void addItem(Item item){
		inventory.add(item);
		if (item.getItemName().length() > maxItemNameLength)
				maxItemNameLength = item.getItemName().length();
	}
	public void subtractItem (Item item, int count){
		for (int i = 0; i < count; i++){
			/*if (inventory.contains(withName(name)));
				inventory.remove(withName(name));*/
			if (inventory.contains(item))
				inventory.remove(item);
		}
	}

	public Item withName(String s){
		//Very sloppy because of laziness.
		for (Item i : inventory){
			if (s.contains(i.getItemName()))
				return i;
		}
		return null;
	}

	public String spacer(String itemName){
		//4 spaces after maxItemNameLength
		while (itemName.length() < maxItemNameLength)
			itemName += " ";

		itemName += "    ";
		return itemName;
	}
	public String spacerSide(String itemName){
		//2 spaces after maxItemNameLength
		while (itemName.length() < maxItemNameLength)
			itemName += " ";

		itemName += "  ";
		return itemName;
	}
}

