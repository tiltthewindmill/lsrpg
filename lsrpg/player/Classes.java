package lsrpg.player;
import lsrpg.battle.*;
import lsrpg.common.*;
import lsrpg.*;

import java.util.ArrayList;
import java.util.Random;

public class Classes{
	private static final int WARRIOR = 1;
	private static final int MAGE = 2;
	private static final int RANGER = 3;
	private static final int RANDOM = 4;
	private static final int IMPORT = 9;
	private static final int CREATE = 11;
	private Game game;

	public Classes(Game game){
		this.game = game;
	}
	//Three default classes, Warrior, Mage, Ranger
	public Player getPlayerClass(int selection){
		Random rand = new Random();
		switch(selection){
			case WARRIOR:
				return new Warrior();
			case MAGE:
				return new Mage();
			case RANGER:
				return new Ranger();
			case RANDOM:
				return getPlayerClass(rand.nextInt(3)+1);
			default:
				// throw new Exception();
				return new Warrior();
		}
	}

}

class Warrior implements Player{

	private int maxHealth = 50;
	private int health = maxHealth;
	private int strength = 15;
	private int magic = 2;
	private int magicDamage = 4;
	private int resistance = 12;

	private String className = "Warrior";
	private String playerName = " ";
	private ArrayList<Attack> attacks = new ArrayList<Attack>();
	private Inventory inventory;

	//Set attacks
	//Name, strength, type
	public Warrior(){
		attacks.add(new Attack("Strike",12,"Melee", this));
		attacks.add(new Attack("Throwing Axe",10,"Cast", this));
		attacks.add(new Attack("Mighty Thwomp",13,"Power", this));

		inventory = new Inventory(this);
		inventory.addItem(ItemFactory.health_potion());
		inventory.addItem(ItemFactory.health_potion());
		inventory.addItem(ItemFactory.key());
	}

	public int getHealth(){
		return health;
	}
	public int getStrength(){
		return strength;
	}
	public int getMagic(){
		return magic;
	}
	public int getMagicDamage(){
		return magicDamage;
	}
	public int getResistance(){
		return resistance;
	}
	public String getClassName(){
		return className;
	}
	public ArrayList<Attack> getAttacks(){
		return attacks;
	}
	public void returnMaxHealth(){
		health = maxHealth;
	}
	public void setPlayerName(String s){
		playerName = s;
	}
	public String getPlayerName(){
		return playerName;
	}
	public void setHealth(int health){
		this.health = health;
	}
	public void subtractHealth(int amt){
		this.health -= amt;
	}
	public String statPrinter(){
		String out = "";
		if (!playerName.equals(" "))
			out += "Name:\t\t"+this.getPlayerName()+"\n";
		out += "Class:\t\t"+this.getClassName()+"\n";
		out += "Health:\t\t"+this.getHealth()+"\n";
		out += "Strength:\t"+this.getStrength()+"\n";
		out += "Magic:\t\t"+this.getMagic()+"\n";
		out += "Magic Damage:\t"+this.getMagicDamage()+"\n";
		out += "Resistance:\t"+this.getResistance()+"\n";
		return out;
	}
	public Inventory getInventory(){
		return inventory;
	}
	public String printInventory(){
		return inventory.print();
	}
	public String printSideInventory(){
		return inventory.sideBarPrint();
	}
	public String useItem (String s){
		String out = "";
		if (inventory.withName(s) != null){
			out += "You have used ";
			Item it = inventory.withName(s);
			out += it.getItemName();
			Modifier mod = it.getModifier();
			if (mod != null){
				if (mod.getType() != Constants.KEY){
					if (mod.getType() == Constants.HEALTH){
						health = health + mod.getAmount();
						if (health > maxHealth)
							health = maxHealth;
					} else if (mod.getType() == Constants.RESIST){
						resistance += mod.getAmount();
					} else if (mod.getType() == Constants.STRENGTH){
						strength += mod.getAmount();
					} else if (mod.getType() == Constants.MAGIC){
						magic += mod.getAmount();
					} else if (mod.getType() == Constants.MAGIC_DAMAGE){
						magicDamage += mod.getAmount();
					}
					out += " and have gained " + mod.getAmount() + " " + mod.getType();
					out += "\nYour health is now: " + health+"\n";
				} 
			}
			inventory.subtractItem(it,1);
		} else{
			return (Constants.NO_ITEM + s);
		}
		return out;
	}
	public void addItem(Item item){
		inventory.addItem(item);
	}

}

class Mage implements Player{

	private int maxHealth = 35;
	private int health = maxHealth;
	private int strength = 9;
	private int magic = 15;
	private int magicDamage = 15;
	private int resistance = 9;

	private String className = "Mage";
	private String playerName = " ";
	private Inventory inventory;
	private ArrayList<Attack> attacks = new ArrayList<Attack>();

	public Mage(){
		attacks.add(new Attack("IceBall",12,"Melee", this));
		attacks.add(new Attack("Barrier",6,"Cast", this));
		attacks.add(new Attack("FireDeath",13,"Power", this));

		inventory = new Inventory(this);
		inventory.addItem(ItemFactory.health_potion());
		inventory.addItem(ItemFactory.health_potion());
		inventory.addItem(ItemFactory.key());

	}

	public int getHealth(){
		return health;
	}
	public int getStrength(){
		return strength;
	}
	public int getMagic(){
		return magic;
	}
	public int getMagicDamage(){
		return magicDamage;
	}
	public int getResistance(){
		return resistance;
	}
	public String getClassName(){
		return className;
	}
	public ArrayList<Attack> getAttacks(){
		return attacks;
	}
	public void returnMaxHealth(){
		health = maxHealth;
	}
	public void setPlayerName(String s){
		playerName = s;
	}
	public String getPlayerName(){
		return playerName;
	}
	public void setHealth(int health){
		this.health = health;
	}
	public void subtractHealth(int amt){
		this.health -= amt;
	}
	public String statPrinter(){
		String out = "";
		if (!playerName.equals(" "))
			out += "Name:\t\t"+this.getPlayerName()+"\n";
		out += "Class:\t\t"+this.getClassName()+"\n";
		out += "Health:\t\t"+this.getHealth()+"\n";
		out += "Strength:\t"+this.getStrength()+"\n";
		out += "Magic:\t\t"+this.getMagic()+"\n";
		out += "Magic Damage:\t"+this.getMagicDamage()+"\n";
		out += "Resistance:\t"+this.getResistance()+"\n";
		return out;
	}
	public Inventory getInventory(){
		return inventory;
	}
	public String printInventory(){
		return inventory.print();
	}
	public String printSideInventory(){
		return inventory.sideBarPrint();
	}
	public String useItem (String s){
		String out = "";
		if (inventory.withName(s) != null){
			out += "You have used ";
			Item it = inventory.withName(s);
			out += it.getItemName();
			Modifier mod = it.getModifier();
			if (mod != null){
				if (mod.getType() != Constants.KEY){
					if (mod.getType() == Constants.HEALTH){
						health = health + mod.getAmount();
						if (health > maxHealth)
							health = maxHealth;
					} else if (mod.getType() == Constants.RESIST){
						resistance += mod.getAmount();
					} else if (mod.getType() == Constants.STRENGTH){
						strength += mod.getAmount();
					} else if (mod.getType() == Constants.MAGIC){
						magic += mod.getAmount();
					} else if (mod.getType() == Constants.MAGIC_DAMAGE){
						magicDamage += mod.getAmount();
					}
					out += " and have gained " + mod.getAmount() + " " + mod.getType();
					out += "\nYour health is now: " + health+"\n";
				}
			}else {
					System.out.println("A KEY WAS USED");
			}
			inventory.subtractItem(it,1);
		} else {
			return (Constants.NO_ITEM + s);
		}
		return out;
	}
	public void addItem(Item item){
		inventory.addItem(item);
	}
}

class Ranger implements Player{

	private int maxHealth = 45;
	private int health = maxHealth;
	private int strength = 12;
	private int magic = 5;
	private int magicDamage = 6;
	private int resistance = 15;

	private String className = "Ranger";
	private String playerName = " ";
	private Inventory inventory;
	private ArrayList<Attack> attacks = new ArrayList<Attack>();

	//Need to add way to alter enemy stats on type of attack..

	public Ranger(){
		attacks.add(new Attack("Dagger Thrust",12,"Melee", this));
		attacks.add(new Attack("Arrow",6,"Cast", this));
		attacks.add(new Attack("Yell",13,"Power", this));

		inventory = new Inventory(this);
		inventory.addItem(ItemFactory.health_potion());
		inventory.addItem(ItemFactory.health_potion());
		inventory.addItem(ItemFactory.key());
	}


	public int getHealth(){
		return health;
	}
	public int getStrength(){
		return strength;
	}
	public int getMagic(){
		return magic;
	}
	public int getMagicDamage(){
		return magicDamage;
	}
	public int getResistance(){
		return resistance;
	}
	public String getClassName(){
		return className;
	}
	public ArrayList<Attack> getAttacks(){
		return attacks;
	}
	public void returnMaxHealth(){
		health = maxHealth;
	}
	public void setPlayerName(String s){
		playerName = s;
	}
	public String getPlayerName(){
		return playerName;
	}
	public void setHealth(int health){
		this.health = health;
	}
	public void subtractHealth(int amt){
		this.health -= amt;
	}
	public String statPrinter(){
		String out = "";
		if (!playerName.equals(" "))
			out += "Name:\t\t"+this.getPlayerName()+"\n";
		out += "Class:\t\t"+this.getClassName()+"\n";
		out += "Health:\t\t"+this.getHealth()+"\n";
		out += "Strength:\t"+this.getStrength()+"\n";
		out += "Magic:\t\t"+this.getMagic()+"\n";
		out += "Magic Damage:\t"+this.getMagicDamage()+"\n";
		out += "Resistance:\t"+this.getResistance()+"\n";
		return out;
	}
	public Inventory getInventory(){
		return inventory;
	}
	public String printInventory(){
		return inventory.print();
	}
	public String printSideInventory(){
		return inventory.sideBarPrint();
	}
	public String useItem (String s){
		String out = "";
		if (inventory.withName(s) != null){
			out += "You have used ";
			Item it = inventory.withName(s);
			out += it.getItemName();
			Modifier mod = it.getModifier();
			if (mod != null){
				if (mod.getType() != Constants.KEY){
					if (mod.getType() == Constants.HEALTH){
						health = health + mod.getAmount();
						if (health > maxHealth)
							health = maxHealth;
					} else if (mod.getType() == Constants.RESIST){
						resistance += mod.getAmount();
					} else if (mod.getType() == Constants.STRENGTH){
						strength += mod.getAmount();
					} else if (mod.getType() == Constants.MAGIC){
						magic += mod.getAmount();
					} else if (mod.getType() == Constants.MAGIC_DAMAGE){
						magicDamage += mod.getAmount();
					}
					out += " and have gained " + mod.getAmount() + " " + mod.getType();
					out += "\nYour health is now: " + health+"\n";
				}
			}
			inventory.subtractItem(it,1);
		} else{
			return (Constants.NO_ITEM + s);
		}
		return out;
	}
	public void addItem(Item item){
		inventory.addItem(item);
	}


	
}