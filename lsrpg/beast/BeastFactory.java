package lsrpg.beast;

import java.util.Random;
import java.util.ArrayList;
import lsrpg.*;
import lsrpg.battle.*;

public class BeastFactory{
	public static final int SKELETON = 1;
	public static final int WOLF = 2;
	public static final int SPIDER = 3;
	private static Random rand;

	public BeastFactory(){
		rand = new Random();

	}

	public static ArrayList<Beast> getBeast(int selection){
		ArrayList<Beast> beastArr = new ArrayList<Beast>();
		switch(selection){
			case SKELETON:
				beastArr.add(new Skeleton());
				break;
			case WOLF:
				beastArr.add(new Wolf());
				break;
			case SPIDER:
				beastArr.add(new Spider());
				break;
		}
		return beastArr;
	}

	public static ArrayList<Beast> randBeast(int limit){
		rand = new Random();
		ArrayList<Beast> beastArr = new ArrayList<Beast>();
		for (int i = 0; i < limit; i++){
			beastArr.add(getBeast(rand.nextInt(3)+1).get(0));
		}
		return beastArr;
	}	
}

class Skeleton implements Beast{

	public int maxHealth = 15;
	public int health = maxHealth;
	public int strength = 3;
	public int resistance = 2;
	public int magic = 2;
	public int magicDamage = 4;
	public String beastName = "Skeleton";
	private ArrayList<Attack> attacks = new ArrayList<Attack>();

	public Skeleton(){
		attacks.add(new Attack("Prod", 10, "Melee"));
		attacks.add(new Attack("Coffin Cry", 3, "Cast"));
		attacks.add(new Attack("Bone Throw", 12, "Power"));
	}


	public int getHealth(){
		return health;
	}
	public int getStrength(){
		return strength;
	}
	public int getMagic(){
		return magic;
	}
	public int getMagicDamage(){
		return magicDamage;
	}
	public int getResistance(){
		return resistance;
	}
	public String getBeastName(){
		return beastName;
	}
	public ArrayList<Attack> getAttacks(){
		return attacks;
	}
	public void returnMaxHealth(){
		health = maxHealth;
	}
	public void setHealth(int health){
		this.health = health;
	}
	public void subtractHealth(int amt){
		this.health -= amt;
		if (this.health < 0)
			this.health = 0;
	}
}

class Wolf implements Beast{

	public int maxHealth = 28;
	public int health = maxHealth;
	public int strength = 3;
	public int resistance = 2;
	public int magic = 2;
	public int magicDamage = 4;
	public String beastName = "Wolf";
	private ArrayList<Attack> attacks = new ArrayList<Attack>();

	public Wolf(){
		attacks.add(new Attack("Scratch", 12, "Melee"));
		attacks.add(new Attack("Howl", 5, "Cast"));
		attacks.add(new Attack("Strike", 13, "Power"));
	}


	public int getHealth(){
		return health;
	}
	public int getStrength(){
		return strength;
	}
	public int getMagic(){
		return magic;
	}
	public int getMagicDamage(){
		return magicDamage;
	}
	public int getResistance(){
		return resistance;
	}
	public String getBeastName(){
		return beastName;
	}
	public ArrayList<Attack> getAttacks(){
		return attacks;
	}
	public void returnMaxHealth(){
		health = maxHealth;
	}
	public void setHealth(int health){
		this.health = health;
	}
	public void subtractHealth(int amt){
		this.health -= amt;
		if (this.health < 0)
			this.health = 0;
	}

}

class Spider implements Beast{

	public int maxHealth = 19;
	public int health = maxHealth;
	public int strength = 3;
	public int resistance = 4;
	public int magic = 2;
	public int magicDamage = 4;
	public String beastName = "Spider";
	private ArrayList<Attack> attacks = new ArrayList<Attack>();

	public Spider(){
		attacks.add(new Attack("Bite", 12, "Melee"));
		attacks.add(new Attack("Acid Spit", 5, "Cast"));
		attacks.add(new Attack("Fling", 13, "Power"));
	}


	public int getHealth(){
		return health;
	}
	public int getStrength(){
		return strength;
	}
	public int getMagic(){
		return magic;
	}
	public int getMagicDamage(){
		return magicDamage;
	}
	public int getResistance(){
		return resistance;
	}
	public String getBeastName(){
		return beastName;
	}
	public ArrayList<Attack> getAttacks(){
		return attacks;
	}
	public void returnMaxHealth(){
		health = maxHealth;
	}
	public void setHealth(int health){
		this.health = health;
	}
	public void subtractHealth(int amt){
		this.health -= amt;
		if (this.health < 0)
			this.health = 0;
	}
}