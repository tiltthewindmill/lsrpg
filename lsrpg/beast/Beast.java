package lsrpg.beast;
import lsrpg.*;
import lsrpg.battle.*;

import java.util.*;

public interface Beast{
	
	public int getHealth();
	public int getStrength();
	public int getMagic();
	public int getMagicDamage();
	public int getResistance();
	public String getBeastName();

	public ArrayList<Attack> getAttacks();
	public void returnMaxHealth();

	public void setHealth(int health);
	public void subtractHealth(int amt);


}