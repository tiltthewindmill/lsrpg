package lsrpg.dungeon;
import lsrpg.common.*;
import lsrpg.player.*;
import lsrpg.*;
import java.util.*;

public class Dungeon{
	public static final int CORR_CHANCE = 4;
	public static final int NEW_CORR_CHANCE = 4;
	public static final int BUILDING_PASSES = 3;
	boolean dungeonComplete;
	ArrayList<Room> rooms;

	//Traversal stuff
	Room currentRoom, startRoom, finalRoom, prevRoom;
	public String roomID = "room_";
	private Game game;
	//Rooms, walls, etc
	public Dungeon(Game game){
		this.game = game;
		// game.gameOut("Building Dungeon...");
		rooms = new ArrayList<Room>();
		dungeonComplete = false;
		//Lets have some random (nice) generation.

		//Corridors are now implicit.
		
		startRoom = new Room("startRoom");
		startRoom.setStart(true);
		Room zeroRoom = new Room(roomID+0);
		startRoom.connectRoom(startRoom.getFreeSide(),zeroRoom,doorLocker());
		prevRoom = startRoom;

		rooms.add(startRoom);
		rooms.add(zeroRoom);

		int roomSize = Constants.dRange(5,25);
		for (int i = 0; i < roomSize; i++){
			rooms.add(new Room(roomID+(i+1)));
		}

		for (int i = 0; i < BUILDING_PASSES; i++){
			for (Room ro : rooms){
				joinRooms(ro);
			}
		}

		// roomBuilder(zeroRoom,1,30);
		int finalChance = Constants.dRange(2,rooms.size()); //Final is never first or second room
		rooms.get(finalChance).setFinal(true);
		Room finalRoom = rooms.get(finalChance);
		currentRoom = startRoom;


		//Add items here, skip finalRoom
		for (Room room : rooms){
			if (room == finalRoom)
				break;
			int chestChance = Constants.d6();
			if (chestChance < 4){
				room.addChest();
				int itemsInChest = Constants.dRange(1,3);
				Item[] itemsForChest = new Item[itemsInChest];
				for (int i = 0; i < itemsForChest.length; i++){
					room.addItem(ItemFactory.randomItem());
				}
			}

			//Lock doors
			int lockChance = Constants.d20();
			if (lockChance == 2 || lockChance == 12)
				room.lockNorth();
			lockChance = Constants.d20();
			if (lockChance == 2 || lockChance == 12)
				room.lockWest();
			lockChance = Constants.d20();
			if (lockChance == 2 || lockChance == 12)
				room.lockEast();
			lockChance = Constants.d20();
			if (lockChance == 2 || lockChance == 12)
				room.lockSouth();
		}


		if (currentRoom.hasChest())
			game.gameOut("There is a chest in this room! [Open Chest]",false);

		game.updateRoomDisplay(currentRoom.print());
	}

		//Check that dungeon is possible to traverse from start to final.


	public boolean move(String direction){
		boolean success = false;
		if (direction.equalsIgnoreCase(Constants.NORTH_REP) || direction.equalsIgnoreCase("N")){
			if (currentRoom.getNorth() == null){
				game.gameOut("There is a wall.");
			} else{
				if(currentRoom.isNorthLocked()){
					game.gameOut("There is a locked door.\n",false);
					game.gameOut("Would you like to unlock it? [y/n]");
					String useKey = game.gameInput();
					if (useKey.equalsIgnoreCase("y") || useKey.equalsIgnoreCase("yes")){
						if (game.playerUseItem(Constants.KEY)){
							currentRoom.unlockNorth();
							game.gameOut("You have unlocked the door.");
						}
					}
				} else{
					game.gameOut("You walk down a hallway.");
					currentRoom = currentRoom.getNorth();
					success = true;
				}
			}
		} else if (direction.equalsIgnoreCase(Constants.SOUTH_REP) || direction.equalsIgnoreCase("S")){
			if (currentRoom.getSouth() == null){
				game.gameOut("There is a wall.");
			} else{
				if(currentRoom.isSouthLocked()){
					game.gameOut("There is a locked door.\n",false);
					game.gameOut("Would you like to unlock it? [y/n]");
					String useKey = game.gameInput();
					if (useKey.equalsIgnoreCase("y") || useKey.equalsIgnoreCase("yes")){
						if (game.playerUseItem(Constants.KEY)){
							currentRoom.unlockSouth();
							game.gameOut("You have unlocked the door.");
						}
					}
				} else{
					game.gameOut("You walk down a hallway.");
					currentRoom = currentRoom.getSouth();
					success = true;
				}
			}
		} else if (direction.equalsIgnoreCase(Constants.EAST_REP) || direction.equalsIgnoreCase("E")){
			if (currentRoom.getEast() == null){
				game.gameOut("There is a wall.");
			} else{
				if(currentRoom.isEastLocked()){
					game.gameOut("There is a locked door.\n",false);
					game.gameOut("Would you like to unlock it? [y/n]");
					String useKey = game.gameInput();
					if (useKey.equalsIgnoreCase("y") || useKey.equalsIgnoreCase("yes")){
						if (game.playerUseItem(Constants.KEY)){
							currentRoom.unlockEast();
							game.gameOut("You have unlocked the door.");
						}
					}
				} else{
					game.gameOut("You walk down a hallway.");
					currentRoom = currentRoom.getEast();
					success = true;
				}
			}
		} else if (direction.equalsIgnoreCase(Constants.WEST_REP) || direction.equalsIgnoreCase("W")){
			if (currentRoom.getWest() == null){
				game.gameOut("There is a wall.");
			} else{
				if(currentRoom.isWestLocked()){
					game.gameOut("There is a locked door.\n",false);
					game.gameOut("Would you like to unlock it? [y/n]");
					String useKey = game.gameInput();
					if (useKey.equalsIgnoreCase("y") || useKey.equalsIgnoreCase("yes")){
						if (game.playerUseItem(Constants.KEY)){
							currentRoom.unlockWest();
							game.gameOut("You have unlocked the door.");
						}
					}
				} else{
					game.gameOut("You walk down a hallway.");
					currentRoom = currentRoom.getWest();
					success = true;
				}
			}
		} else if (direction.equalsIgnoreCase(Constants.OPEN_CHEST) || direction.equalsIgnoreCase("OC")){
			if (!currentRoom.hasChest())
				game.gameOut("There is no chest here.");
			else{
				if (currentRoom.isChestOpen()){
					game.gameOut("This chest has already been opened.");
				} else{
					game.gameOut("You open the chest.");
					game.givePlayerItems(currentRoom.getItems());
					for (Item item : currentRoom.getItems()){
						game.gameOut("You have received a "+item.getItemName());
					}
					game.updateInventory();
					currentRoom.setChestOpen(true);
					game.updateRoomDisplay(currentRoom.print());
					success = true;
				}
			}

		}

		if (currentRoom.isWestLocked() && currentRoom.isNorthLocked() && currentRoom.isEastLocked() && currentRoom.isSouthLocked()){
			game.gameOut("Uh-oh, you are trapped!\nUnless you have a key...");
		}

		if (currentRoom.isFinal()){
			game.gameOut("Congratulations! You have reached the end.\nRejoice!");
			dungeonComplete = true;
		}

		if (prevRoom != currentRoom){
			prevRoom = currentRoom;
			if (currentRoom.hasChest())
				game.gameOut("There is a chest in this room! [Open Chest]");
			
			game.updateRoomDisplay(currentRoom.print());
		}
		return success;
	}	

	public boolean joinRooms(Room ro){
		int chance = -1;
		Room connector = getUnconnectedRoom();
		if (connector == null)
			return false;
		while (connector == ro){
			connector = getUnconnectedRoom();
		}

		chance = Constants.dSize(12);

		if ((chance == 0 || chance == 4) && ro.getNorth() == null){
			ro.connectRoom(Constants.NORTH_REP,connector,doorLocker());
		} else if ((chance == 1 || chance == 5) && ro.getSouth() == null){
			ro.connectRoom(Constants.SOUTH_REP,connector,doorLocker());
		} else if ((chance == 2 || chance == 6) && ro.getWest() == null){
			ro.connectRoom(Constants.SOUTH_REP,connector,doorLocker());
		} else if ((chance == 3 || chance == 7) && ro.getNorth() == null){
			ro.connectRoom(Constants.SOUTH_REP,connector,doorLocker());
		}

		return true;
	}

	public String getOpposite(String dir){
		if (dir.equals(Constants.NORTH_REP))
			return Constants.SOUTH_REP;
		else if (dir.equals(Constants.SOUTH_REP))
			return Constants.NORTH_REP;
		else if (dir.equals(Constants.WEST_REP))
			return Constants.EAST_REP;
		else if (dir.equals(Constants.EAST_REP))
			return Constants.WEST_REP;

		return "";
	}


	public static boolean doorLocker(){
		int chance = Constants.d6();
		return false;
	}

	public String toString(){
		String toString = "";
		toString += "#Dungeon\n";
		if (rooms == null)
			return null;

		for (Room room : rooms){
			Room north = room.getNorth();
			Room south = room.getSouth();
			Room east = room.getEast();
			Room west = room.getWest();

			toString += "Room: "+room.getID()+"\n";

			toString += "Doors:"+"\n";
			toString += "North: ";
			if (north != null){
				toString += north.getID();
				if (room.isNorthLocked())
					toString += "*\n";
				else
					toString += "\n";
			} else
				toString += "--\n";


			toString += "South: ";
			if (south != null){
				toString += south.getID();
				if (room.isSouthLocked())
					toString += "*\n";
				else
					toString += "\n";
			} else
				toString += "--\n";

			toString += "East: ";
			if (east != null){
				toString += east.getID();
				if (room.isEastLocked())
					toString += "*\n";
				else
					toString += "\n";
			} else
				toString += "--\n";

			toString += "West: ";
			if (west != null){
				toString += west.getID();
				if (room.isWestLocked())
					toString += "*\n";
				else
					toString += "\n";
			} else
				toString += "--\n";


			toString += "Start: ";
			if (room.isStart())
				toString += "Yes"+"\n";
			else
				toString += "No"+"\n";

			toString += "Finish: ";
			if (room.isFinal())
				toString += "Yes"+"\n";
			else
				toString += "No"+"\n";

			toString += "\n\n";
		}
		return toString;
	}

	public boolean isDungeonComplete(){
		return dungeonComplete;
	}

	public Room getUnconnectedRoom(){
		ArrayList<Room> uncoRooms = new ArrayList<Room>();
		for (Room r : rooms){
			if (r.getNumberOfDoors() > 0){
				uncoRooms.add(r);
			}
		}
		if (uncoRooms.size() == 0)
			return null;

		return uncoRooms.get(Constants.dSize(uncoRooms.size()));
	}

	public static String[][] addNorthPath (String[][] arr){
		arr[0][9] = "|";
		arr[0][10] = " ";
		arr[0][11] = "|";
		return arr;
	}
	public static String[][] addSouthPath (String[][] arr){
		arr[10][9] = "|";
		arr[10][10] = " ";
		arr[10][11] = "|";
		return arr;
	}
	public static String[][] addWestPath (String[][] arr){
		arr[4][0] = "-";
		arr[5][0] = " ";
		arr[6][0] = "_";
		return arr;
	}
	public static String[][] addEastPath (String[][] arr){
		arr[4][20] = "-";
		arr[5][20] = " ";
		arr[6][20] = "_";
		return arr;
	}

	public static String[][] addOpenChest (String[][] arr){
		arr[3][8] = "_";
		arr[3][9] = "/";
		arr[4][8] = "|";
		arr[4][9] = "_";
		arr[4][10] = "_";
		arr[4][11] = "|";
		return arr;
	}

	public static String[][] addClosedChest(String[][] arr){
		arr[3][8] = "_";
		arr[3][9] = "_";
		arr[3][10] = "_";
		arr[3][11] = "_";
		arr[4][8] = "|";
		arr[4][9] = "_";
		arr[4][10] = "_";
		arr[4][11] = "|";
		return arr;
	}

	public static String[][] addFinalSymbol(String[][] arr){
		arr[4][9] = "F";
		arr[4][10] = "I";
		arr[4][11] = "N";
		arr[4][12] = "!";
		return arr;
	}
}

class Room {
	public Room north, south, east, west;
	public boolean lockedN, lockedS, lockedE, lockedW;	//Locked doors
	public boolean startRoom, finalRoom;
	public String id;
	public String[][] roomPrint;
	public boolean chest, chestOpen;
	public ArrayList<Item> chestItems;
	//If [N,E,S,W] == null implies wall

	public Room(String id){
		north = null;
		south = null;
		east = null;
		west = null;
		lockedN = false;
		lockedS = false;
		lockedW = false;
		lockedE = false;
		startRoom = false;
		finalRoom = false;
		this.id = id;
		roomPrint = new String[11][22];
		for (int i = 0; i < 11; i++){
			for (int j = 0; j < 22; j++){
				if (i == 0 || i == 10){
					roomPrint[i][j] = "*";
				} else{
					roomPrint[i][j] = " ";
				}
				if (j == 0 || j == 20)
					roomPrint[i][j] = "*";

				if (j == 21)
					roomPrint[i][j] = "\n";
			}
		}
		chest = false;
		chestOpen = false;
		chestItems = new ArrayList<Item>();
	}

	public void setStart(boolean start){
		startRoom = start;
	}

	public void setFinal(boolean fin){
		finalRoom = fin;
	}

	public boolean isStart(){
		return startRoom;
	}

	public boolean isFinal(){
		return finalRoom;
	}

	public String getID(){
		return id;
	}

	public boolean isNorthLocked(){
		return lockedN;
	}
	public boolean isSouthLocked(){
		return lockedS;
	}
	public boolean isWestLocked(){
		return lockedW;
	}
	public boolean isEastLocked(){
		return lockedE;
	}

	public void connectRoom(String direction, Room room, boolean locked){
		if (direction.equalsIgnoreCase(Constants.NORTH_REP)){
			if (north == null){
				if (room != null){
					if (room.getNumberOfDoors() > 0){
						north = room;
						room.connecting(room.getFreeSide(),this,Dungeon.doorLocker());
						lockedN = locked;
					} 
				} else{
					north = null;
					lockedN = locked;
				}
			}
		} else if (direction.equalsIgnoreCase(Constants.SOUTH_REP)){
			if (south == null){
				if (room != null){
					if (room.getNumberOfDoors() > 0){
						south = room;
						room.connecting(room.getFreeSide(),this,Dungeon.doorLocker());
						lockedS = locked;
					}
				}else {
					south = null;
					lockedS = locked;
				}
			}
		} else if (direction.equalsIgnoreCase(Constants.EAST_REP)){
			if (east == null){
				if (room != null){
					if (room.getNumberOfDoors() > 0){
						east = room;
						room.connecting(room.getFreeSide(),this,Dungeon.doorLocker());
						lockedE = locked;
					}
				} else {
					east = null;
					lockedE = locked;
				}
			}
		} else if (direction.equalsIgnoreCase(Constants.WEST_REP)){
			if (west == null){
				if (room != null){
					if (room.getNumberOfDoors() > 0){
						west = room;
						room.connecting(room.getFreeSide(),this,Dungeon.doorLocker());
						lockedW = locked;
					}
				} else {
					west = null;
					lockedW = locked;
				}
			}
		}
	}

	public void connecting(String direction, Room room, boolean locked){
		if (direction.equalsIgnoreCase(Constants.NORTH_REP)){
			if (north == null){
				north = room;
				lockedN = locked;
			}
		} else if (direction.equalsIgnoreCase(Constants.SOUTH_REP)){
			if (south == null){
				south = room;
				lockedS = locked;
			}
		} else if (direction.equalsIgnoreCase(Constants.EAST_REP)){
			if (east == null){
				east = room;
				lockedE = locked;
			}
		} else if (direction.equalsIgnoreCase(Constants.WEST_REP)){
			if (west == null){
				west = room;
				lockedW = locked;
			}
		}
	}

	public int getNumberOfDoors(){
		int count = 0;
		if (north == null)
			count++;

		if (south == null)
			count++;

		if (east == null)
			count++;

		if (west == null)
			count++;

		return count;
	}

	public String getFreeSide(){
		int cc = Constants.dSize(4);
		if (cc == 0){
			if (north==null)
				return Constants.NORTH_REP;
			else if (east==null)
				return Constants.EAST_REP;
			else if (south==null)
				return Constants.SOUTH_REP;
			else if (west==null)
				return Constants.WEST_REP;
		} else if (cc == 1){
			if (south==null)
				return Constants.SOUTH_REP;
			else if (east==null)
				return Constants.EAST_REP;
			else if (north==null)
				return Constants.NORTH_REP;
			else if (west==null)
				return Constants.WEST_REP;
		} else if (cc == 2){
			if (east==null)
				return Constants.EAST_REP;
			else if (west==null)
				return Constants.WEST_REP;
			else if (south==null)
				return Constants.SOUTH_REP;
			else if (west==null)
				return Constants.WEST_REP;
		} else if (cc == 3){
			if (west==null)
				return Constants.WEST_REP;
			else if (north==null)
				return Constants.NORTH_REP;
			else if (south==null)
				return Constants.SOUTH_REP;
			else if (east==null)
				return Constants.EAST_REP;
		}

		return "";
	}

	public Room getNorth(){
		return north;
	}

	public Room getSouth(){
		return south;
	}

	public Room getEast(){
		return east;
	}

	public Room getWest(){
		return west;
	}

	public String print(){
		if (north != null){
			roomPrint = Dungeon.addNorthPath(roomPrint);
		}
		if (south != null){
			roomPrint = Dungeon.addSouthPath(roomPrint);
		}
		if (east != null){
			roomPrint = Dungeon.addEastPath(roomPrint);
		}
		if (west != null){
			roomPrint = Dungeon.addWestPath(roomPrint);
		}

		if (chest && chestOpen){
			roomPrint = Dungeon.addOpenChest(roomPrint);
		}

		if (chest && !chestOpen){
			roomPrint = Dungeon.addClosedChest(roomPrint);
		}

		if (finalRoom)
			roomPrint = Dungeon.addFinalSymbol(roomPrint);

		String out = "";
		for (int i = 0; i < 11; i++){
			for (int j = 0; j < 22; j++){
				out += roomPrint[i][j];
			}
		}
		return out;
	}

	public void addChest(){
		chest = true;
	}

	public boolean hasChest(){
		return chest;
	}

	public boolean isChestOpen(){
		return chestOpen;
	}

	public void setChestOpen(boolean bo){
		chestOpen = bo;
	}

	public void addItem(Item item){
		chestItems.add(item);
	}

	public ArrayList<Item> getItems(){
		return chestItems;
	}

	public void unlockNorth(){
		lockedN = false;
	}

	public void unlockSouth(){
		lockedS = false;
	}
	public void unlockWest(){
		lockedW = false;
	}
	public void unlockEast(){
		lockedE = false;
	}

	public void lockNorth(){
		lockedN = true;
	}
	public void lockSouth(){
		lockedS = true;
	}
	public void lockWest(){
		lockedW = true;
	}
	public void lockEast(){
		lockedE = true;
	}

}