package lsrpg.common;

import lsrpg.*;
import lsrpg.player.*;
/**
 * An item.
 * Has a name and a modifier.
 * The name is what the TYPE of the potion e.g. Health.
 */
public class Item {

	private String itemName;
	private Modifier modifier = null;

	public Item(String itemName, Modifier modifier){
		this.itemName = itemName;
		this.modifier = modifier;
	}

	public String getItemName(){
		return itemName;
	}
	public Modifier getModifier(){
		return modifier;
	}

}