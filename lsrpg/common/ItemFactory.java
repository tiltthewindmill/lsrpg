package lsrpg.common;

import lsrpg.*;
import lsrpg.player.*;

/**
 * Makes Items for different reasons such as health, strength...
 */
public class ItemFactory{

	public ItemFactory(){}

	//Items
	public static Item health_potion(){
		return new Item("Health Potion", new Modifier(Constants.HEALTH,12));
	}

	public static Item strength_potion(){
		return new Item("Strength Potion", new Modifier(Constants.STRENGTH,5));
	}

	public static Item resistance_potion(){
		return new Item("Resistance Potion", new Modifier(Constants.RESIST,10));
	}

	public static Item magic_potion(){
		return new Item("Magic Potion", new Modifier(Constants.MAGIC,5));
	}

	public static Item magicDamage_potion(){
		return new Item("Magic Damage Potion", new Modifier(Constants.MAGIC_DAMAGE,5));
	}

	public static Item key(){
		return new Item("Key", new Modifier(Constants.KEY,0));
	}

	public static Item randomItem(){
		int rand = Constants.dSize(9);
		switch (rand){
			case 0:
				return health_potion();
			case 1:
				return strength_potion();
			case 2: 
				return resistance_potion();
			case 3:
				return magic_potion();
			case 4:
				return magicDamage_potion();
			default:
				return key();
		}
	}




}