package lsrpg.common;

import lsrpg.*;
import lsrpg.player.*;
import lsrpg.battle.*;
import lsrpg.dungeon.*;

public class Modifier{

	//Types of modifiers:
	/*
		Cast+
		Melee+
		Power+
		Cast-
		Melee-
		Power-
		Stats +/-
		Enemy stats +/-
	*/
	private String modifyType;
	private int modifyAmount;

	public Modifier(String m, int ma){
		modifyType = m;
		modifyAmount = ma;
	}

	public String getType(){
		return modifyType;
	}
	public int getAmount(){
		return modifyAmount;
	}
}