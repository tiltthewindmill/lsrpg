package lsrpg.common;

import java.util.Random;

public class Constants{

	//Menu Constants
	public static final int CONTINUE_CUR = 0;
	public static final int NEW_RAND_RPG = 1;
	public static final int NEW_RAND_ADV = 2;
	public static final int NEW_SCRI_RPG = 2;
	public static final int NEW_SCRI_ADV = 4;
	public static final int LOAD_SAV_GAM = 3;
	public static final int SAVE_CUR_GAM = 4;
	public static final int QUIT_GAME = 9;
	public static final String NORTH_REP = "North";
	public static final String SOUTH_REP = "South";
	public static final String EAST_REP = "East";
	public static final String WEST_REP = "West";
	public static final String OPEN_CHEST = "Open Chest";

	public static final String MELEE = "Melee";
	public static final String CAST = "Cast";
	public static final String POWER = "Power";

	//Modifier types
	public static final String HEALTH = "Health";
	public static final String STRENGTH = "Strength";
	public static final String MAGIC = "Magic";
	public static final String RESIST = "Resistance";
	public static final String MAGIC_DAMAGE = "Magic Damage";

	public static final String ENEMY_HEALTH = "Enemy Health";
	public static final String ENEMY_STRENGTH = "Enemy Strength";
	public static final String ENEMY_MAGIC = "Enemy Magic";
	public static final String ENEMY_RESIST = "Enemy Resistance";
	public static final String ENEMY_MAGIC_DAMAGE = "Enemy Magic Damage";

	public static final String KEY = "Key";
	public static final String NO_ITEM = "You do not have any ";
	

	public static final String HELP_COMMANDS = "All commands start with a :\n:i - Brings up your inventory.\n:m - Brings up the main menu.\n:s - Brings up your stats\n:? - Brings up this menu.\n:q - Quits the game.";
	public static final String MORE_HELP = HELP_COMMANDS +"\nTo use an item type:\n:i use <Item Name>\n";

	//Room drawing
	public static final String EMPTY_ROOM = 
				"*********************\n"+
				"*                   *\n"+
				"*                   *\n"+
				"*                   *\n"+
				"*                   *\n"+
				"*                   *\n"+
				"*                   *\n"+
				"*                   *\n"+
				"*                   *\n"+
				"*                   *\n"+
				"*********************";

	public static final String STORY_START_1 = "You awaken in a dimly lit room. You have very little memory of the events that transpired the night before.  All you know is that you have to get out of here.";
	public static final String STORY_START_2 = "Blinding lights everywhere. You feel around the room for a way out.";
	public static final String STORY_START_3 = "You decided to give up everything and become a professional dungeon raider. You like killing beasts and wandering around in the dark.";

	public static final String ALL_STORY_START = "All you have is your trusty weapon, the clothes on your back and a couple of potions and keys.";

	public static final String INTRODUCTION = "This is an attempt to make a (much) better version of a game I made a couple of years ago. Fight monsters, collect items and use your imagination!"
		+"\nInformation:\nNot all rooms are connected by, for example, by South -> North. South from one room may take you to the East entrance of another.\nIf you think you may be stuck in a loop, you probably are."
			+"\nThe aim is to get from the starting room to the finish room without dying. Harder than it sounds! ;)";

	public static final String VERSION_NOTES = "Version 0.9: Locked doors can now be unlocked!!  Dungeon is generated via a finite random method.";

	public static Random rand = null;

	//Pause for time milliseconds
	public static void pause(int time){
		try{
			Thread.sleep(time);
		} catch (java.lang.InterruptedException e){
			e.printStackTrace();
		}
	}
	
	public static void commandArrow(){
		System.out.print("> ");
	}

	//Print lines blank-lines
	public static String pad(int lines){
		String out = "";
		for (int i = 0; i < lines; i++){
			out += "\n";
		}
		return out;
	}

	public static void randInit(){
		if (rand == null)
			rand = new Random();
	}

	public static int d20(){
		randInit();
		return rand.nextInt(20) +1;
	}
	public static int d6(){
		randInit();
		return rand.nextInt(6) +1;
		// return 1;
	}
	public static int d32(){
		randInit();
		return rand.nextInt(32) +1;
	}

	public static int dSize(int size){
		randInit();
		return rand.nextInt(size);
	}

	public static int dRange(int lower, int upper){
		randInit();
		int randAmount = rand.nextInt(upper);
		if (randAmount < lower)
			randAmount += lower;

		return randAmount;
	}

	/**
	 * Handles all input commands
	 * @param  inp The input from the command box
	 * @return     The appropriate full command or the original command itself
	 */
	public static String inputHandler(String inp){
		if (inp.equalsIgnoreCase(":q")){
			System.out.println("Good-bye");
			System.exit(0);
		} else if (inp.equalsIgnoreCase(":m")){
			int sel = Menu.printMenu();
			if (sel == 0){
				return "";
			}
		} else if (inp.equalsIgnoreCase(":i")){
			return "inventory";
		} else if (inp.startsWith(":i")) {
			return "inventoryUse";
		} else if (inp.equalsIgnoreCase(":?")){
			return "printHelp";
		} else if (inp.equalsIgnoreCase(":s") || inp.equalsIgnoreCase(":stats")){
			return "printStats";
		}

		return inp;
	}
}