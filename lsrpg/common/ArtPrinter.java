package lsrpg.common;

import lsrpg.*;

public class ArtPrinter{
	private int sleepTime = 100;
	private Game game;
	public ArtPrinter(Game game){
		this.game = game;
	}


	public void print(String art){
		//Change to switch...
		if (art.equals("DemonSkull")){
			game.gameOut("          .                                                      .",false);
			Constants.pause(sleepTime);
			game.gameOut("        .n                   .                 .                  n.",false);
			Constants.pause(sleepTime);
			game.gameOut("  .   .dP                  dP                   9b                 9b.    .",false);
			Constants.pause(sleepTime);
			game.gameOut(" 4    qXb         .       dX                     Xb       .        dXp     t",false);
			Constants.pause(sleepTime);
			game.gameOut("dX.    9Xb      .dXb    __                         __    dXb.     dXP     .Xb",false);
			Constants.pause(sleepTime);
			game.gameOut("9XXb._       _.dXXXXb dXXXXbo.                 .odXXXXb dXXXXb._       _.dXXP",false);
			Constants.pause(sleepTime);
			game.gameOut(" 9XXXXXXXXXXXXXXXXXXXVXXXXXXXXOo.           .oOXXXXXXXXVXXXXXXXXXXXXXXXXXXXP",false);
			Constants.pause(sleepTime);
			game.gameOut("  `9XXXXXXXXXXXXXXXXXXXXX'~   ~`OOO8b   d8OOO'~   ~`XXXXXXXXXXXXXXXXXXXXXP'",false);
			Constants.pause(sleepTime);
			game.gameOut("    `9XXXXXXXXXXXP' `9XX'          `98v8P'          `XXP' `9XXXXXXXXXXXP'",false);
			Constants.pause(sleepTime);
			game.gameOut("        ~~~~~~~       9X.          .db|db.          .XP       ~~~~~~~",false);
			Constants.pause(sleepTime);
			game.gameOut("                        )b.  .dbo.dP'`v'`9b.odb.  .dX(",false);
			Constants.pause(sleepTime);
			game.gameOut("                      ,dXXXXXXXXXXXb     dXXXXXXXXXXXb.",false);
			Constants.pause(sleepTime);
			game.gameOut("                     dXXXXXXXXXXXP'   .   `9XXXXXXXXXXXb",false);
			Constants.pause(sleepTime);
			game.gameOut("                    dXXXXXXXXXXXXb   d|b   dXXXXXXXXXXXXb",false);
			Constants.pause(sleepTime);
			game.gameOut("                    9XXb'   `XXXXXb.dX|Xb.dXXXXX'   `dXXP",false);
			Constants.pause(sleepTime);
			game.gameOut("                     `'      9XXXXXX(   )XXXXXXP      `'",false);
			Constants.pause(sleepTime);
			game.gameOut("                              XXXX X.`v'.X XXXX",false);
			Constants.pause(sleepTime);
			game.gameOut("                              XP^X'`b   d'`X^XX",false);
			Constants.pause(sleepTime);
			game.gameOut("                              X. 9  `   '  P )X",false);
			Constants.pause(sleepTime);
			game.gameOut("                              `b  `       '  d'",false);	
			Constants.pause(sleepTime);		
			game.gameOut("Artist: Unknown",false);
		}
	}
}