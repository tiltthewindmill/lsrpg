package lsrpg.common;
import lsrpg.*;
import java.util.*;
/**
*
*	This prints the menu and detects options.
*
*
*/
public class Menu{
	private static int selection = -1;
	private static Scanner input = new Scanner(System.in);
	private static Game game;
	public Menu(Game game){
		this.game = game;
	}

	public static int printMenu(){
		game.gameOut("************MAIN MENU************",false);
		game.gameOut("0: Continue",false);
		game.gameOut("1: New Random RPG",false);
		game.gameOut("2: New Scripted RPG (Coming soon)",false);
		game.gameOut("3: Load Saved Game (Coming soon)",false);
		game.gameOut("4: Save Current Game (Coming soon)",false);
		game.gameOut("9: Quit",false);
		Constants.commandArrow();
		selection = Integer.parseInt(game.gameInput());

		switch (selection){
			case Constants.CONTINUE_CUR:
				selection = 0;
				break;
			case Constants.NEW_RAND_RPG:
				selection = 1;
				break;
			case Constants.NEW_SCRI_RPG:
				selection = 2;
				break;
			case Constants.LOAD_SAV_GAM:
				selection = 3;
				break;
			case Constants.SAVE_CUR_GAM:
				selection = 4;
				break;
			case Constants.QUIT_GAME:
				selection = 9;
				break;
		}
		return selection;
	}

	public static int printMainMenu(){
		game.gameOut("************MAIN MENU************",false);
		game.gameOut("1: New Random RPG",false);
		game.gameOut("2: New Scripted RPG (Coming soon)",false);
		game.gameOut("3: Load Saved Game (Coming soon)",false);
		game.gameOut("9: Quit",false);
		Constants.commandArrow();
		selection = Integer.parseInt(game.gameInput());

		switch (selection){
			case Constants.NEW_RAND_RPG:
				selection = 1;
				break;
			case Constants.NEW_SCRI_RPG:
				selection = 2;
				break;
			case Constants.LOAD_SAV_GAM:
				selection = 3;
				break;
			case Constants.SAVE_CUR_GAM:
				selection = 4;
				break;
			case Constants.QUIT_GAME:
				selection = 9;
				break;
		}
		return selection;
	}

	public int getSelection(){
		return selection;
	}


}