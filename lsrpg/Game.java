package lsrpg;

import lsrpg.*;
import lsrpg.battle.*;
import lsrpg.player.*;
import lsrpg.dungeon.*;
import lsrpg.beast.*;
import lsrpg.common.*;

import java.util.*;

public class Game{
	private static Menu menu;
	private static GUI gui;
	private Scanner textInput, input;
	private Random rand;
	private ArtPrinter artPrinter;
	private Classes classesFactory;
	private Player player;
	private Battle battle;
	private boolean	playerIsAlive, gameInProgress, selectionBool;

	public Game(GUI guig){
		
		//Input
		this.gui = null;
		this.gui = guig;
		input = new Scanner(System.in);
		textInput = new Scanner(System.in);
		rand = new Random();
		battle = new Battle(this);
		artPrinter = new ArtPrinter(this);
		classesFactory = new Classes(this);
		int selection = 0;
		menu = new Menu(this);
		String inputParsing = "";
		
		playerIsAlive = true;
		gameInProgress = true;
		selectionBool = true;

		Constants.pad(6);
		
		//Header
		gameOut("//////////// **********WELCOME TO LACHLAN'S STUPID TEXT RPG********** ////////////",false);
		gameOut("////////////////// Based on Lachlan's Stupid Text RPG circa 2010 /////////////////",false);
		gameOut("////////////////// Current:  Nov 2012. lachlan@tiltthewindmill.net ///////////////",false);
		gameOut("////////////////////////////// Version : 0.90 ////////////////////////////////////",false);
		Constants.pause(250);

		//Print something cool.
		artPrinter.print("DemonSkull");

		Constants.pause(500);
		gameOut(Constants.pad(3),false);
		gameOut(Constants.INTRODUCTION+"\n",false);
		gameOut(Constants.VERSION_NOTES+"\n",false);
		Constants.pause(1000);

		if (gui != null)
			gui.setHelp(Constants.MORE_HELP);

		
		while (gameInProgress){
			while (selectionBool){
				try{
					selection = menu.printMainMenu();
					selectionBool = false;
				} catch (java.lang.NumberFormatException e){
					gameOut("Please enter a valid input.");
				}
			}
				selectionBool = true;
			if (selection == 9){
				System.exit(0);
			}
			if (selection == 1){
				gameOut(Constants.pad(3),false);

				gameOut("Select your class:",false);
				gameOut("Warrior: 1    Mage: 2    Ranger: 3    Random: 4",false);
				// Constants.commandArrow();
				gameOut("",false);
				while (selectionBool){
					try{
						selection = Integer.parseInt(gameInput());
						selectionBool = false;
					} catch (java.lang.NumberFormatException e){
						gameOut("Please enter a valid input.");
					}
				}
				selectionBool = true;
				// selection = Integer.parseInt(gameInput());

				player = classesFactory.getPlayerClass(selection);

				//Stat Display
				gameOut("Your stats are: ",false);
				gameOut(player.statPrinter(),false);
				

				// Name Entry
				gameOut("Please enter the hero's name:",false);
				// Constants.commandArrow();
				gameOut("",true);
				String playerName = gameInput();
				player.setPlayerName(playerName);
				updatePlayerStats(player.statPrinter());
				updateInventory(player.printSideInventory());
				
				gameOut("",false);
				gameOut("Ahh, so it is " + player.getPlayerName() + " the " + player.getClassName() + "\n",false);
				Constants.pause(1000);

				int storyStartRand = Constants.dSize(3);
				switch (storyStartRand){
					case 0:
						gameOut(Constants.STORY_START_1);
						break;
					case 1:
						gameOut(Constants.STORY_START_2);
						break;
					case 2:
						gameOut(Constants.STORY_START_3);
						break;
				}
				gameOut(Constants.ALL_STORY_START+"\n");

				
				Dungeon dungeon = new Dungeon(this);

				//The main game loop//
				while (!dungeon.isDungeonComplete()){
					gameOut("Which way do you want to move? [N,E,S,W]",false);
					// Constants.commandArrow();
					gameOut("",true);
					inputParsing = gameInput();
					String nextCommand = Constants.inputHandler(inputParsing);
					if (parseDirections(nextCommand)){
						boolean success = dungeon.move(inputParsing);
						int chance = Constants.dRange(1,8);
						if (!dungeon.isDungeonComplete()){
							if (success){
								if (chance <= 2){
									//Battle
									ArrayList<Beast> beasts = BeastFactory.randBeast(Constants.dRange(1,4));
									battle.newBattle(player,beasts);
								}
							} else {
								if (chance == 1){
									//Battle
									ArrayList<Beast> beasts = BeastFactory.randBeast(Constants.dRange(1,4));
									battle.newBattle(player,beasts);
								}
							}
						}
					}else if (nextCommand.equals("inventory")){
						gameOut(player.printInventory(),false);
					}else if (nextCommand.equals("inventoryUse")){
						gameOut(player.useItem(inputParsing.substring(2,inputParsing.length())),false);
						updateInventory(player.printSideInventory());
						updatePlayerStats(player.statPrinter());
					}else if (nextCommand.equals("printStats")){
						gameOut(player.statPrinter(),false);
					}else if (nextCommand.equals("printHelp")){
						gameOut("******HELP******");
						gameOut(Constants.MORE_HELP,false);
					}

					//Need a command validator
					gameOut(Constants.pad(1),false);

					if (!playerIsAlive){
						gameOut("You have died. :(");
						break;
					}

				}
				gameOut("Would you like to play again? [y/n]",false);
				String again = gameInput();
				player = null;
				playerIsAlive = true;
				if (again.equalsIgnoreCase("no") || again.equalsIgnoreCase("n")){
					gameInProgress = false;
				}
				
			}
		}

		gameOut("\n*******************************",false);
		gameOut("Thanks For Playing LSRPG!\nBye!\n\nContact: lachlan@tiltthewindmill.net |||| Twitter: @lollbirdsey\nTiltingWindmillsStudios.net",false);
		gameOut("*******************************",false);
		Constants.pause(7654);
		System.exit(0);

		
		
	}

	/**
	 * Check to see if a direction command is valid
	 * @param  s The command
	 * @return   True if valid, false if not.
	 */
	public static boolean parseDirections(String s){
		if (s.equalsIgnoreCase(Constants.NORTH_REP) || s.equalsIgnoreCase("N"))
			return true;
		if (s.equalsIgnoreCase(Constants.SOUTH_REP) || s.equalsIgnoreCase("S"))
			return true;
		if (s.equalsIgnoreCase(Constants.WEST_REP) || s.equalsIgnoreCase("W"))
			return true;
		if (s.equalsIgnoreCase(Constants.EAST_REP) || s.equalsIgnoreCase("E"))
			return true;
		if (s.equalsIgnoreCase(Constants.OPEN_CHEST) || s.equalsIgnoreCase("OC"))
			return true;

		return false;
	}

	public static void parseCommands(String s){
		if (s.equalsIgnoreCase(":q")){
			gameOut("Good-bye");
			System.exit(0);
		} else if (s.equalsIgnoreCase(":m")){
			int sel = menu.printMenu();
			if (sel == 0){
				return;
			}
		} else if (s.equalsIgnoreCase(":i"))
		{
			gameOut("Inventory:");
		}
	}

	public static void gameOut(String s){
		if (gui == null)
			System.out.println("> " + s);
		else
			gui.appendText("> " + s);
	}

	public static void gameOut(String s, boolean arrow){
		if (arrow){
			if (gui == null)
				System.out.print("> " + s);
			else
				gui.appendText("> " + s);
		} else {
			if (gui == null)
				System.out.println(s);
			else
				gui.appendText(s);
		}
	}

	public String gameInput(){
		if (gui == null){
			return textInput.nextLine();
		} else {
			String inText = gui.getInputText();
			gameOut(inText);
			return inText;
		}
	}
	public void setGUI(GUI gui){
		this.gui = gui;
	}

	public void updateRoomDisplay(String s){
		if (gui != null){
			gui.updateRoomDisplay(s);
		}
	}
	public void updatePlayerStats(String s){
		if (gui != null){
			gui.updatePlayerStats(s);
		}
	}

	public void updateInventory(String s){
		if (gui != null){
			gui.updateInventory(s);
		}
	}

	public void updateInventory(){
		updateInventory(player.printSideInventory());
	}

	public void givePlayerItems(ArrayList<Item> arr){
		for (int i = 0; i < arr.size(); i++){
			player.addItem(arr.get(i));
		}
	}

	public boolean playerUseItem(String item){
		String s = player.useItem(item);
		if (s.contains(Constants.NO_ITEM)){
			gameOut(s);
			return false;
		}else{
			updateInventory();
			return true;
		}

	}

	public void playerDied(){
		playerIsAlive = false;
	}
}