package lsrpg;

import lsrpg.common.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.JFrame;
import javax.swing.BorderFactory;
import javax.swing.text.DefaultCaret;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Font;
import java.awt.Dimension;
import java.awt.BorderLayout;


import java.awt.Frame;

public class GUI extends JFrame implements ActionListener{
	
	private static final Font FONT = new Font("Courier",Font.PLAIN, 14);
	private static final Font HEADER_FONT = new Font("Courier",Font.BOLD,14);
	JPanel mainPanel, rightPanel, upRightPanel, downRightPanel, middleRightPanel, downDownRightPanel,spacing;
	JTextField input;
	JTextArea text, roomDisplay, playerStats, inventory, command;
	JScrollPane mainScroll;
	JLabel roomHeader, statsHeader, inventoryHeader, commandHeader;
	Game game;
	ArrayList<String> busyWait = new ArrayList<String>();

	public GUI(){	
		super("Lachlan's Stupid RPG");
		setExtendedState(Frame.MAXIMIZED_BOTH);
		spacing = new JPanel();
		spacing.setBackground(Color.black);
		spacing.setForeground(Color.black);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		mainPanel = new JPanel(new BorderLayout());
		mainPanel.setBackground(Color.black);
		rightPanel = new JPanel(new BorderLayout());
		input = new JTextField(97);
		// input.setPreferredSize(new Dimension(5,97));
		text = new JTextArea();

		//Current Location window
		upRightPanel = new JPanel();
		upRightPanel.setBackground(Color.black);
		roomHeader = new JLabel();
		roomHeader.setFont(HEADER_FONT);
		roomHeader.setBackground(Color.black);
		roomHeader.setForeground(Color.white);
		roomHeader.setText("Current Location: ");

		roomDisplay = new JTextArea();
		roomDisplay.setFont(FONT);
		roomDisplay.setBackground(Color.black);
		roomDisplay.setForeground(Color.white);
		roomDisplay.setEditable(false);

		//Current stats window
		middleRightPanel = new JPanel();
		middleRightPanel.setBackground(Color.black);
		statsHeader = new JLabel();
		statsHeader.setFont(HEADER_FONT);
		statsHeader.setBackground(Color.black);
		statsHeader.setForeground(Color.white);
		statsHeader.setText("Current Stats: ");

		playerStats = new JTextArea();
		playerStats.setFont(FONT);
		playerStats.setBackground(Color.black);
		playerStats.setForeground(Color.white);
		playerStats.setEditable(false);

		//Inventory window
		downRightPanel = new JPanel();
		inventoryHeader = new JLabel();
		inventoryHeader.setFont(HEADER_FONT);
		inventoryHeader.setBackground(Color.black);
		inventoryHeader.setForeground(Color.white);
		inventoryHeader.setText("Inventory: ");

		inventory = new JTextArea();
		inventory.setFont(FONT);
		inventory.setBackground(Color.black);
		inventory.setForeground(Color.white);
		inventory.setEditable(false);

		//Commands window
		downDownRightPanel = new JPanel();
		commandHeader = new JLabel();
		commandHeader.setFont(HEADER_FONT);
		commandHeader.setBackground(Color.black);
		commandHeader.setForeground(Color.white);
		commandHeader.setText("Commands: ");

		command = new JTextArea();
		command.setFont(FONT);
		command.setBackground(Color.black);
		command.setForeground(Color.white);
		command.setEditable(false);



	
		//Main text window
		text.setFont(FONT);
		text.setBackground(Color.black);
		text.setForeground(Color.white);
		text.setWrapStyleWord(true);
		text.setLineWrap(true);
		text.setEditable(false);
		//Main text window - now with scroll bars
		mainScroll = new JScrollPane(text);
		mainScroll.setBackground(Color.black);
		mainScroll.setBorder(BorderFactory.createEmptyBorder());

		input.addActionListener(this);
		

		setBackground(Color.black);

		// mainScroll = new JScrollPane(text);
		// mainScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		// text.setCaretPosition(text.getDocument().getLength()); // PRE Java5
		DefaultCaret caret = (DefaultCaret)text.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		rightPanel.add(upRightPanel, BorderLayout.NORTH);
		upRightPanel.add(roomHeader);
		upRightPanel.add(roomDisplay);
		rightPanel.add(middleRightPanel, BorderLayout.CENTER);
		middleRightPanel.add(statsHeader);
		middleRightPanel.add(playerStats);
		middleRightPanel.add(downRightPanel,BorderLayout.SOUTH);
		// rightPanel.add(downRightPanel, BorderLayout.SOUTH);
		downRightPanel.add(inventoryHeader);
		downRightPanel.add(spacing);
		downRightPanel.add(inventory);

		rightPanel.add(downDownRightPanel, BorderLayout.SOUTH);
		downDownRightPanel.setPreferredSize(new Dimension(250,200));
		downDownRightPanel.add(commandHeader);
		downDownRightPanel.add(command);
		downDownRightPanel.setBackground(Color.black);
		downDownRightPanel.setForeground(Color.white);

		rightPanel.setPreferredSize(new Dimension(250,800));

		upRightPanel.setPreferredSize(new Dimension(250,300));
		upRightPanel.setBackground(Color.black);
		upRightPanel.setForeground(Color.white);

		middleRightPanel.setPreferredSize(new Dimension(250,200));
		middleRightPanel.setBackground(Color.black);
		middleRightPanel.setForeground(Color.white);

		downRightPanel.setPreferredSize(new Dimension(250,200));
		downRightPanel.setBackground(Color.black);
		downRightPanel.setForeground(Color.white);

		mainPanel.add(rightPanel, BorderLayout.LINE_END);
		mainPanel.add(input, BorderLayout.PAGE_END);
		// mainPanel.add(text,BorderLayout.CENTER);
		mainPanel.add(mainScroll);
		mainPanel.setBackground(Color.black);
		rightPanel.setBackground(Color.white);


		setContentPane(mainPanel);
		getContentPane().setBackground(Color.black);
		input.requestFocusInWindow();
		upRightPanel.setVisible(true);
		mainPanel.setVisible(true);
		rightPanel.setVisible(true);
		setVisible(true);
		input.requestFocus();
	}

	public void actionPerformed(ActionEvent e){
		if (e.getSource() == input){
			//stuff
			//text.append(input.getSourcetText() + "\n");
			synchronized(busyWait){
				busyWait.add(input.getText());
				busyWait.notify();
			}
			input.setText("");
		}
		// text.setCaretPosition(text.getDocument().getLength());
	}

	public void appendText(String s){
		text.append(s +"\n");
	}

	public String getInputText(){
		synchronized (busyWait){
			
			while (busyWait.isEmpty()){
				try{
					busyWait.wait();
				} catch (java.lang.InterruptedException e){
					game.gameOut(e.toString());
					game.gameOut("An error has occurred. Please contact: ");
				}
			}

			return busyWait.remove(0);
		}
		//return text.getText();
	}

	public void setGame(Game game){
		this.game = game;
	}

	public void updateRoomDisplay(String s){
		roomDisplay.setText(s);
	}

	public void updatePlayerStats(String s){
		playerStats.setText(s);
	}

	public void updateInventory(String s){
		inventory.setText(s);
	}

	public void setHelp(String s){
		command.setText(s);
	}
}