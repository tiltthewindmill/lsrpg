package lsrpg.battle;
import lsrpg.*;
import lsrpg.player.*;
import lsrpg.beast.*;
import lsrpg.common.*;

import java.util.*;

public class Combat{
	Random rand;
	Game game;
	public Combat(Game game){
		rand = new Random();
		this.game = game;
	}

	//Combat modifiers are applied in here.
	public void nextMove(Attack pAttack, Player player, Beast beast){
		int beastHealth = beast.getHealth();
		int playerHealth = player.getHealth();
		game.gameOut("You have used "+pAttack.getAttackName()+".",false);
		//Modifier stuff

		int acc = Constants.d6();
		double hit, bResist;
		bResist = beast.getResistance();
		hit = -1;
		String attackType = pAttack.getAttackType();

		if (attackType == Constants.MELEE){
			switch (acc){
				case 5:
					//Miss or glancing hit?
					acc = Constants.d6();
					if (acc <4){
						//Glancing
						hit = (player.getStrength()/2) + pAttack.getAttackStrength();
						hit = hit / 3;
						bResist = beast.getResistance() * 1.2;
						game.gameOut("Glancing!",false);
					} else{
						game.gameOut("Your attack missed.",false);
						hit = -1;
					}
					break;
				case 6:
					hit = player.getStrength() + (player.getStrength()/3) + pAttack.getAttackStrength() + 1;
					bResist = beast.getResistance() * 1.2;
					break;
				default:
					//Normal hit
					hit = (player.getStrength()/2) + pAttack.getAttackStrength() - acc;
					bResist = beast.getResistance() * 1.2;
					break;
			}
		} else if (attackType == Constants.CAST){
			switch (acc){
				case 5:
					//Miss or glancing hit?
					acc = Constants.d6();
					if (acc <4){
						//Glancing
						hit = (player.getMagicDamage()/2) + (player.getMagic()/2) + (pAttack.getAttackStrength()/2);
						hit = hit / 3;
						bResist = beast.getResistance() * (beast.getMagic()/4);
						game.gameOut("Glancing!",false);
					} else{
						game.gameOut("Your attack missed.",false);
						hit = -1;
					}
					break;
				case 6:
					hit = (player.getMagicDamage()/2) + (player.getMagic()/2) + (pAttack.getAttackStrength()/2) + (player.getMagicDamage()/2);
					bResist = beast.getResistance() * (beast.getMagic()/4);
					break;
				default:
					//Normal hit()
					hit = (player.getMagicDamage()/2) + (player.getMagic()/2) + (pAttack.getAttackStrength()/2) - acc;
					bResist = beast.getResistance() * (beast.getMagic()/4);
					break;
			}
		} else if (attackType == Constants.POWER){
			switch (acc){
				case 5:
					//Miss or glancing hit?
					acc = Constants.d6();
					if (acc <4){
						//Glancing
						hit = (player.getStrength()/2) + pAttack.getAttackStrength();
						hit = hit / 3;
						bResist = beast.getResistance() * 1.2;
						game.gameOut("Glancing!",false);
					} else{
						game.gameOut("Your attack missed.",false);
						hit = -1;
					}
					break;
				case 6:
					hit = player.getStrength() + (player.getStrength()/3) + pAttack.getAttackStrength() + 1;
					bResist = beast.getResistance() * 1.2;
					break;
				default:
					//Normal hit
					hit = (player.getStrength()/2) + pAttack.getAttackStrength() - acc;
					bResist = beast.getResistance() * 1.2;
					break;
			}
		}

		if (hit >= 0){
			hit = hit - (bResist/3);
			if (hit < 0)
				hit = 0;
			beast.subtractHealth((int)hit);
			game.gameOut("Your "+pAttack.getAttackName()+ " has hit the " + beast.getBeastName() + " for "+ (int)hit + " points.\n",false);
		}

		game.gameOut("----"+beast.getBeastName()+"'s turn: ",false);
		Constants.pause(1000);

		acc = Constants.d6();
		//Enemy attack time.
		double pResist = player.getResistance();
		hit = -1;

		Attack bAttack = beast.getAttacks().get(Constants.dSize(beast.getAttacks().size()));	//Random attack (CHANGE LATER KJASHDUA!@IUHDI:)
		attackType = bAttack.getAttackType();
		game.gameOut("The "+beast.getBeastName()+" has used "+bAttack.getAttackName() +" on you.",false);
		if (attackType == Constants.MELEE){
			switch (acc){
				case 5:
					//Miss or glancing hit?
					acc = Constants.d6();
					if (acc <4){
						//Glancing
						hit = (beast.getStrength()/2) + bAttack.getAttackStrength();
						hit = hit / 3;
						pResist = beast.getResistance() * 1.5;
						game.gameOut("Glancing!",false);
					} else{
						game.gameOut("Your attack missed.\n",false);
						hit = -1;
					}
					break;
				case 6:
					hit = beast.getStrength() + (beast.getStrength()/3) + bAttack.getAttackStrength() + 1;
					pResist = player.getResistance() * 1.5;
					break;
				default:
					//Normal hit
					hit = beast.getStrength() + bAttack.getAttackStrength() - acc +1;
					pResist = player.getResistance() * 1.5;
					break;
			}
		} else if (attackType == Constants.CAST){
			switch (acc){
				case 5:
					//Miss or glancing hit?
					acc = Constants.d6();
					if (acc <4){
						//Glancing
						hit = (beast.getMagicDamage()/2) + (beast.getMagic()/2) + (bAttack.getAttackStrength()/2);
						hit = hit / 3;
						pResist = player.getResistance() * (player.getMagic()/4);
						game.gameOut("Glancing!",false);
					} else{
						game.gameOut("Your attack missed.",false);
						hit = -1;
					}
					break;
				case 6:
					hit = (beast.getMagicDamage()/2) + (beast.getMagic()/2) + (bAttack.getAttackStrength()/2) + (beast.getMagicDamage()/2);
					pResist = player.getResistance() * (player.getMagic()/4);
					break;
				default:
					//Normal hit()
					hit = (beast.getMagicDamage()/2) + (beast.getMagic()/2) + (bAttack.getAttackStrength()/2) - acc+1;
					pResist = player.getResistance() * (player.getMagic()/4);
					break;
			}
		} else if (attackType == Constants.POWER){
			switch (acc){
				case 5:
					//Miss or glancing hit?
					acc = Constants.d6();
					if (acc <4){
						//Glancing
						hit = (beast.getStrength()/2) + bAttack.getAttackStrength();
						hit = hit / 3;
						pResist = player.getResistance() * 1.5;
						game.gameOut("Glancing!",false);
					} else{
						game.gameOut("Your attack missed.",false);
						hit = -1;
					}
					break;
				case 6:
					hit = beast.getStrength() + (beast.getStrength()/3) + bAttack.getAttackStrength() + 1;
					pResist = player.getResistance() * 1.5;
					break;
				default:
					//Normal hit
					hit = (beast.getStrength()/2) + bAttack.getAttackStrength() - acc+1;
					pResist = player.getResistance() * 1.5;
					break;
			}
		}
		if (hit >= 0){
			hit = hit - (pResist/3);
			if (hit < 0)
				hit = 0;
			
			player.subtractHealth((int)hit);
			game.gameOut("The "+beast.getBeastName()+"'s "+bAttack.getAttackName() +" has hit you for "+(int)hit+" points.\n",false);
		}
	}

	
}