package lsrpg.battle;
import lsrpg.*;

import lsrpg.player.*;
import lsrpg.common.*;
import lsrpg.beast.*;

import java.util.ArrayList;
import java.util.Scanner;

public class Battle{
	int combatSelection;
	String inputLine;
	Scanner input = new Scanner(System.in);
	Game game;

	public Battle (Game game){
		this.game = game;
	}
	public void newBattle(Player player, ArrayList<Beast> beasts){
		Combat combat = new Combat(game);
		this.game = game;
		ArrayList<Attack> playerAttacks = player.getAttacks();
		game.gameOut("--------BATTLE!--------",false);
		Constants.pause(300);
		if (beasts != null){
			game.gameOut("You have encountered "+beasts.size()+" beasts!",false);
		}
		
		while (beasts.size() > 0){
			
			game.gameOut("\nA " + beasts.get(0).getBeastName() +" has appeared! Get ready!",false);
			Constants.pause(1000);

			while (beasts.get(0).getHealth() > 0 && player.getHealth() > 0){
				//Print commands for action
				Attack pAttack = null;
				while (pAttack == null){
					String pAttacks = "";
					for (int i = 0; i < playerAttacks.size(); i++){
						pAttacks += i+1 +": "+playerAttacks.get(i).getAttackName()+"  ";

					}
					game.gameOut(pAttacks);
					game.gameOut("",true);
					// Constants.commandArrow();
					inputLine = game.gameInput();
					inputLine.trim();
					String inputClone = inputLine;
					inputLine = Constants.inputHandler(inputLine);
					try{
						//Test to see if int was selected
						combatSelection = Integer.parseInt(inputLine);
						combatSelection--;
						if (combatSelection > playerAttacks.size() - 1 || combatSelection < 0){
							game.gameOut("Please select action again.",false);
						} else {
							pAttack = playerAttacks.get(combatSelection).getAttack();
						}

					} catch (NumberFormatException e){
						//Check if attack name was typed in.
						for (Attack at : playerAttacks){
							if (inputLine.equalsIgnoreCase(at.getAttackName())){
								pAttack = at;
								break;
							}
						}
						String nextCommand = Constants.inputHandler(inputClone);

						if (nextCommand.equals("inventory")){
							game.gameOut(player.printInventory(),false);
						}else if (nextCommand.equals("inventoryUse")){
							game.gameOut(player.useItem(inputClone.substring(2,inputClone.length())),false);
							game.updatePlayerStats(player.statPrinter());
							game.updateInventory();
						}else if (nextCommand.equals("printStats")){
							game.gameOut(player.statPrinter(),false);
						} else if (nextCommand.equals("help")){
							game.gameOut(Constants.MORE_HELP,false);
						} else {
							game.gameOut("Please select action again.",false);
						}
					}
				}

				combat.nextMove(pAttack,player,beasts.get(0));

				//Print health
				game.gameOut("You have "+player.getHealth() + " health left.",false);
				game.gameOut("The "+ beasts.get(0).getBeastName() +" has "+beasts.get(0).getHealth() + " health left",false);
				if (beasts.get(0).getHealth() <= 0){
					game.gameOut("You have killed the " + beasts.get(0).getBeastName()+".",false);
					beasts.remove(0);
					break;
				}
				game.updatePlayerStats(player.statPrinter());
				game.gameOut(Constants.pad(1),false);
			}

			//Post-battle
			game.updatePlayerStats(player.statPrinter());
			if (beasts.size() <= 0)
				break;
			if (player.getHealth() <= 0){
				game.playerDied();
				break;
			}
		}
		//Post-battle
			game.updatePlayerStats(player.statPrinter());
			if (player.getHealth() <= 0){
				game.playerDied();
			}

	}
}