package lsrpg.battle;

import lsrpg.beast.*;
import lsrpg.*;
import lsrpg.player.*;
import lsrpg.common.*;
/**
*
*	This defines types of attacks
*	Attacks are created in Player declaration.
*
*/

public class Attack{

	private String attackName;
	private int attackStrength;	/*ints*/
	private String attackType;	/*Melee,Cast,Power*/
	private Player player;

	//Stat increases,decreases. ||MODIFIERS||//
	//Attack messages 

	public Attack(String a, int as, String at, Player p){
		attackName = a;
		player = p;
		attackStrength = as;
		if (at.equalsIgnoreCase(Constants.MELEE) || at.equalsIgnoreCase(Constants.CAST) || at.equalsIgnoreCase(Constants.POWER))
			attackType = at;
		/*else
			throw new Exception();	//Replace with own.*/
	}

	public Attack(String a, int as, String at){
		attackName = a;
		attackStrength = as;
		if (at.equalsIgnoreCase(Constants.MELEE) || at.equalsIgnoreCase(Constants.CAST) || at.equalsIgnoreCase(Constants.POWER))
			attackType = at;
		/*else
			throw new Exception();	//Replace with own.*/
	}

	public String getAttackName(){
		return attackName;
	}

	public int getAttackStrength(){
		return attackStrength;
	}

	public String getAttackType(){
		return attackType;
	}

	public void setModifier(){
		//something
	}
	public Attack getAttack(){
		return this;
	}
}
